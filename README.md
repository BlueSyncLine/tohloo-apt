tohloo-apt
==========

`tohloo-apt` is a program for demodulation of NOAA APT (automatic picture transmission) signals.

After building using the Makefile, you can use it together with SoX in order to demodulate a recording: `sox my-apt-recording.wav -t raw -e signed-integer -b 16 -r 48000 -c 1 - | ./apt > apt.data`.

The file `apt.data` will contain the raw demodulated samples.

Afterwards, `aptsync` can be used to properly align the lines: `./aptsync apt.data apt-synced.data`.

The file `apt-synced.data` can be treated as a 8bpp greyscale image (at a resolution of 2080*x, where x is the duration of the recording in lines).

For example, it can be imported into GIMP (hence the `.data` extension):
* Image type: `Indexed`
* Width: `2080`
* Height: move the slider to the maximum possible value to select the entire duration of the recording.

