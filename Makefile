all: clean apt aptsync

clean:
	rm -f apt aptsync

apt:
	gcc -O2 -Wall -Wextra -lm apt.c -o apt

aptsync:
	gcc -O2 -Wall -Wextra sync.c -o aptsync
