// Finds the sync pulses in demodulated APT data and corrects the offset.
#include <stdlib.h>
#include <stdio.h>

#define APT_LINE 2080
#define APT_SYNCLEN 40

// TODO what if the sync is split across two lines? (extremely unlikely, but still)
const unsigned char APT_SYNC[APT_SYNCLEN] = {
    // Front gap
    11, 11, 11, 11,

    // 7 periods of 1040 Hz
    244, 244, 11, 11,
    244, 244, 11, 11,
    244, 244, 11, 11,
    244, 244, 11, 11,
    244, 244, 11, 11,
    244, 244, 11, 11,
    244, 244, 11, 11,

    // Back gap
    11, 11, 11, 11,
    11, 11, 11, 11
};

int main(int argc, char *argv[]) {
    int i, j, best_error, best_pos, error, peak[APT_LINE], peak_index, peak_value;
    unsigned char line[APT_LINE];

    FILE *infile, *outfile;

    // Check the arguments.
    if (argc < 3) {
        fprintf(stderr, "Usage: %s <infile> <outfile>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Try opening the input file.
    if (!(infile = fopen(argv[1], "rb"))) {
        perror("couldn't open the input file");
        return EXIT_FAILURE;
    }

    // Try opening the output file.
    if (!(outfile = fopen(argv[2], "wb"))) {
        perror("couldn't open the output file");
        fclose(infile);
        return EXIT_FAILURE;
    }

    // Initialize the peak array.
    for (i = 0; i < APT_LINE; i++)
        peak[i] = 0;

    // Read the lines until EOF.
    while (fread(line, 1, APT_LINE, infile) == APT_LINE) {
        best_error = APT_SYNCLEN * 255;
        best_pos = 0;

        for (i = 0; i < APT_LINE - APT_SYNCLEN; i++) {
            error = 0;

            // Compute the overall error.
            for (j = 0; j < APT_SYNCLEN; j++)
                error += abs(((int)line[i + j]) - APT_SYNC[j]);

            // New best point found.
            if (error < best_error) {
                best_error = error;
                best_pos = i;
            }
        }

        // Increment the peak value.
        peak[best_pos]++;
    }

    // Find the peak index.
    peak_index = 0;
    peak_value = 0;

    for (i = 0; i < APT_LINE; i++) {
        if (peak[i] > peak_value) {
            peak_value = peak[i];
            peak_index = i;
        }
    }

    // Debug
    fprintf(stderr, "peak found at offset %d\n", peak_index);

    // Seek to the offset where the presumed sync point is.
    fseek(infile, peak_index, SEEK_SET);

    // Copy the lines over.
    while (fread(line, 1, APT_LINE, infile) == APT_LINE)
        fwrite(line, 1, APT_LINE, outfile);

    // Close the files, exit.
    fflush(outfile);
    fclose(outfile);
    fclose(infile);

    return EXIT_SUCCESS;
}
