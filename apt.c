#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <math.h>
#include <complex.h>


// APT parameters.
#define APT_FRAMELEN    2080
#define APT_FRAMES_PER_SECOND 2
#define APT_RATE     (APT_FRAMELEN * APT_FRAMES_PER_SECOND)

#define INPUT_RATE   48000
#define INPUT_BUFFER_SIZE 256

#define CARRIER_FREQ 2400.0f
#define PLL_LIMIT       2.5f
#define PLL_BANDWIDTH   2.0f

#define AGC_COEFF       0.1f


// Computes the decay coefficient for an IIR low-pass filter.
float iir_coeff(float cutoff, float sample_rate) {
    //float y = 1.0f - cosf(2.0f * M_PI * cutoff / sample_rate);
    //return sqrtf(y*y + 2.0f*y) - y;

    // A simpler approximation, since the above loses precision for low cutoff frequencies.
    return 1.0f - expf(-2.0f * M_PI * cutoff / sample_rate);
}


// Converts a float to an uint8_t (clamping)
uint8_t clamp_uint8(float x) {
    if (x > 255.0f) {
        x = 255.0f;
    } else if (x < 0.0f) {
        x = 0.0f;
    }

    return (uint8_t)x;
}


int main(int argc, char *argv[]) {
    int i, j, t;
    int16_t buf[INPUT_BUFFER_SIZE];
    float complex pll_vco, pll_out, pll_prev;
    float demod_buf[APT_FRAMELEN], carrier_filter_coeff, demod_filter_coeff, resamp_phase, samp, demod_filter, demod_prev, pll_freq, pll_error, pll_filter_coeff, vco_phase;
    float frame_min, frame_max, frame_min_mean, frame_max_mean;
    uint8_t frame_samp;

    // Initialize the variables.
    t = 0;
    demod_filter = 0.0f;
    demod_prev = 0.0f;
    resamp_phase = 0.0f;
    pll_freq = CARRIER_FREQ;
    pll_out = 0.0f;
    pll_prev = 0.0f;
    vco_phase = 0.0f;
    frame_min_mean = 0.0f;
    frame_max_mean = 0.0f;

    // Compute the coefficient for the demod filters and the PLL.
    carrier_filter_coeff = iir_coeff(PLL_BANDWIDTH, INPUT_RATE);
    pll_filter_coeff = iir_coeff(PLL_BANDWIDTH, INPUT_RATE);
    demod_filter_coeff = iir_coeff(APT_RATE / 8.0f, INPUT_RATE);

    // Read the input samples.
    while (fread(buf, sizeof(int16_t), INPUT_BUFFER_SIZE, stdin) == INPUT_BUFFER_SIZE) {
        for (i = 0; i < INPUT_BUFFER_SIZE; i++) {
            samp = buf[i] / 32768.0f;

            // Update the PLL.
            pll_vco = cexpf(-I * vco_phase);
            vco_phase += pll_freq * M_PI * 2.0f / INPUT_RATE;
            vco_phase = fmod(vco_phase, M_PI * 2.0f);

            pll_out += carrier_filter_coeff * (samp * pll_vco - pll_out);
            pll_error = cargf(pll_out * conjf(pll_prev)) * INPUT_RATE / M_PI / 2.0f;
            pll_prev = pll_out;
            pll_freq += pll_filter_coeff * pll_error;

            // Clamp the PLL frequency.
            if (pll_freq > CARRIER_FREQ + PLL_LIMIT) {
                pll_freq = CARRIER_FREQ + PLL_LIMIT;
            } else if (pll_freq < CARRIER_FREQ - PLL_LIMIT) {
                pll_freq = CARRIER_FREQ - PLL_LIMIT;
            }

            // Demodulation
            demod_filter += demod_filter_coeff * (cabsf(samp * pll_vco) - demod_filter);

            // Resampler step (depends on the frequency the PLL's locked at).
            // Was APT_RATE * 2.0f
            resamp_phase += (pll_freq / CARRIER_FREQ * APT_RATE) / INPUT_RATE;

            // Sampling point
            if (resamp_phase >= 1.0f) {
                resamp_phase -= 1.0f;

                // Interpolate the demodulated sample.
                demod_buf[t++] = demod_filter * (1.0f - resamp_phase) + demod_prev * resamp_phase;

                // The demodulation buffer now contains a full APT line, scale and output it.
                if (t == APT_FRAMELEN) {
                    t = 0;

                    // Find the maximum sample in the buffer.
                    frame_max = 0.0f;
                    for (j = 0; j < APT_FRAMELEN; j++) {
                        if (demod_buf[j] > frame_max)
                            frame_max = demod_buf[j];
                    }

                    // Find the minimum value.
                    frame_min = frame_max;
                    for (j = 0; j < APT_FRAMELEN; j++) {
                        if (demod_buf[j] < frame_min)
                            frame_min = demod_buf[j];
                    }

                    // Average.
                    frame_max_mean += AGC_COEFF * (frame_max - frame_max_mean);
                    frame_min_mean += AGC_COEFF * (frame_min - frame_min_mean);

                    // Write the frame.
                    fprintf(stderr, "frame: max %f, min %f, pll %f\n", frame_max_mean, frame_min_mean, pll_freq);
                    for (j = 0; j < APT_FRAMELEN; j++) {
                        frame_samp = clamp_uint8((demod_buf[j] - frame_min_mean) / (frame_max_mean - frame_min_mean) * 255.0f);
                        fwrite(&frame_samp, sizeof(uint8_t), 1, stdout);
                    }
                }
            }

            // Preserve the old value of the demodulation filter.
            demod_prev = demod_filter;
        }
    }

    return EXIT_SUCCESS;
}
